<?php

namespace App\Http\Controllers;



use App\Models\BioTime;
use Illuminate\Support\Facades\Log;
use GuzzleHttp\Client;

class BioTimeController extends Controller
{
    private $biotimeUrl = 'http://172.168.250.250:8081/'; // Hardcoded BioTime URL
    private $biotimeUsername = 'test'; // Hardcoded BioTime username
    private $biotimePassword = 'test123456'; // Hardcoded BioTime password

    public function fetchDataAndSave()
    {
        try {
            $biotimeUrl = $this->biotimeUrl;
            $employeeUrl = $biotimeUrl . 'personnel/api/employees/';
    
            // Prepare the headers for the employee data request
            $headers = [
                'Authorization' => $this->getToken(), // Set the Authorization header with the JWT token
            ];
    
            // Create a Guzzle HTTP client
            $client = new Client(['headers' => $headers]);
    
            $response = $client->get($employeeUrl);
    
            if ($response->getStatusCode() !== 200) {
                // Handle the API request failure here
                Log::error('API request failed with status code: ' . $response->getStatusCode());
                return redirect()->back()->with('error', 'Failed to fetch data from BioTime API.');
            }
    
            $employeeData = json_decode($response->getBody(), true);

            if (isset($employeeData['data']) && is_array($employeeData['data'])) {
                do {
                    foreach ($employeeData['data'] as $employee) {
                        
                        // Check if the required keys exist in $employee before accessing them
                        $emp_id = isset($employee['emp_code']) ? $employee['emp_code'] : null;
                        $emp_name = isset($employee['first_name']) ? $employee['first_name'] : null;
                        $emp_dept = isset($employee['department']['dept_name']) ? $employee['department']['dept_name'] : null;
    
                        // Use the emp_id as a unique identifier to sync records
                        $existingBioTime = BioTime::where('emp_id', $emp_id)->first();
    
                        if ($existingBioTime) {
                            // Update existing record
                            $existingBioTime->update([
                                'emp_name' => $emp_name,
                                'emp_id' => $emp_id,
                                'department' => $emp_dept,
                                
                                // Update other fields as needed
                            ]);
                        } else {
                            // Create a new BioTime record with the retrieved data
                            BioTime::create([
                                'emp_id' => $emp_id,
                                'emp_name' => $emp_name,
                                'department' => $emp_dept,
                            ]);
                        }
                    }
                    $nextLink = $employeeData['next'];
    
                    // Fetch the next page of data if the 'next' link exists
                    if ($nextLink) {
                        $response = $client->get($nextLink);
                        $employeeData = json_decode($response->getBody(), true);
                    }
                } while ($nextLink);
            }
    
            return redirect('/')->with('success', 'Data from BioTime API synced with the database.');
        } catch (\Exception $e) {
            // Handle exceptions here
            Log::error('Exception occurred: ' . $e->getMessage());
            return redirect()->back()->with('error', 'An error occurred while fetching and syncing data from BioTime API.');
        }
    }
    private function getToken()
    {
        $biotimeUrl = $this->biotimeUrl;
        $authUrl = $biotimeUrl . 'jwt-api-token-auth/';

        // Create a Guzzle HTTP client for the token retrieval request
        $client = new Client();

        // Prepare the form data for the token retrieval request
        $formData = [
            'username' => $this->biotimeUsername,
            'password' => $this->biotimePassword,
        ];

        $response = $client->post($authUrl, [
            'form_params' => $formData,
        ]);

        if ($response->getStatusCode() !== 200) {
            // Handle the API request failure here
            Log::error('API request failed with status code: ' . $response->getStatusCode());
            throw new \Exception('API request failed');
        }

        $tokenData = json_decode($response->getBody(), true);

        $token = 'JWT ' . $tokenData['token'];
        return $token;
    }
}
