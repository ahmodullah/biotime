<?php

namespace App\Http\Controllers;

use App\Models\BioTime;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Artisan;

class FrontendController extends Controller
{
    public function index(){

        Artisan::call('biotime:fetch-data');
        $employees = BioTime::latest()->paginate(20);
        return view('welcome',compact('employees'));
    }
}
