<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Http\Controllers\BioTimeController;

class FetchBioTimeData extends Command
{
    protected $signature = 'biotime:fetch-data';
    protected $description = 'Fetch and save data from BioTime API';

    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        $bioTimeController = new BioTimeController();
        $bioTimeController->fetchDataAndSave();
        $this->info('Data from BioTime API fetched and saved to the database.');
    }
}
